const {getConfig} = require('../utils/get-config')
const sha512 = require('js-sha512');



/**
 *  generate signature with config credentials
 *  Signing with SHA512 HMAC algorithm
 *  payload is message body to encrypt, must be sorted following alphabet
 *  */
exports.getSignature =  async (payload) =>  {
    const {apiSecret} = getConfig('client')
    const signature = sha512.hmac(apiSecret, payload);
    return {
        signature
    }
}


/**
 *  Returning apikey from config with prefix TDAX-API
 *  
 *  */
exports.getApiKey = async () => {
    const {apiKey} = getConfig('client')
    return {
        apiKey : `TDAX-API ${apiKey}`
    }
}

/**
 * Returning API HOST
 */
exports.getHost = async ()  => {
    const {host} = getConfig('client')
    console.log('host = ', host)
    return {
        host
    }
}

/**
 * Returing user id
 */
exports.getUserID = async () => {
    const {userID} = getConfig('client')
    return {
        userID
    }
}