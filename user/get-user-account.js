const axios = require('axios')
const {getHost,getUserID,getApiKey,getSignature} = require('../client/client')

exports.getUserAccount = async () =>{
    const {host} = await getHost()
    const {userID} = await getUserID()
    const {apiKey} = await getApiKey()
    const {signature} = await getSignature("")
    return axios.get(`${host}/users/${userID}`,{
        headers: {
            Authorization: apiKey,
            Signature: signature
        }
    })
}