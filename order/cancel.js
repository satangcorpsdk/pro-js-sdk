const axios = require('axios')
const {getHost,getApiKey,getSignature} = require('../client/client')

/**
 * Request param
 * order_id: return when order is created
 * pair
 * Return nothing.
 */
exports.cancelOrder = async ({
    orderID,
    pair
}) => {
    const {host} = await getHost()
    const {apiKey} = await getApiKey()
    const {signature} = await getSignature (`pair=${pair}`)
    return axios.delete(`${host}/orders/${orderID}`,{
        headers: {
            Authorization: apiKey,
            Signature: signature,
            'Content-Type': 'application/json'
        },
        data: {
            "pair": pair
        }
    })
}