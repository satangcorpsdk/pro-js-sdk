const axios = require('axios')
const {getHost,getApiKey,getSignature} = require('../client/client')

/**
 * Request param
 * pair: following this format => btc_thb, eth_thb, xrp_thb ...
 * status: posible value ['open','close']
 * limit: limit per page
 * offset: pagination start with 0
 * Returning  example
 [
    {
      id: 716405,
      price: '500000',
      amount: '0.001',
      remaining_amount: '0.001',
      side: 'sell',
      cost: '0',
      created_at: '2018-10-01T10:19:22.296368+02:00',
      status: 'created'
    },
]
 * */
exports.listOrder = async ({
    pair,
    status,
    side,
    limit,
    offset
}) => {
    const {host} = await getHost()
    const {apiKey} = await getApiKey()
    const {signature} = await getSignature ("")
    return axios.get(`${host}/orders/user`,{
        headers: {
            Authorization: apiKey,
            Signature: signature
        },
        params: {
            pair,
            status,
            side,
            limit,
            offset
        }
      })
}