const axios = require('axios')
const {getHost,getApiKey,getSignature} = require('../client/client')

/**
 * Request param
 * type : posible value ['limit', 'market']
 * pair: following this format => btc_thb, eth_thb, xrp_thb ...
 * side : posible value ['sell','buy']
 * nonce : generate from unix time stamp
 * price : price want to create
 * amount : amount want to create
 * 
 * Returning  example
        data: { 
            id: 21358949,
            type: 'limit',
            side: 'sell',
            pair: 'btc_thb',
            open_cost: '0.0015',
            value: '0',
            cost: '0',
            fee_percent: '0.25',
            vat_percent: '7',
            status: 'processing',
            user_id: 145519,
            remote: false,
            created_at: '2019-10-08T12:45:48.662256+02:00',
            updated_at: '2019-10-08T12:45:48.662256+02:00',
            price: '251500',
            amount: '0.0015',
            remain_amount: '0.0015'
        }
    }
 */

 
exports.createOrder = async ({
    type,
    pair,
    side,
    price,
    amount
}) => {
    const nonce = new Date().getTime()
    const payload = `amount=${amount}&nonce=${nonce}&pair=${pair}&price=${price}&side=${side}&type=${type}`
    const {host} = await getHost()
    const {apiKey} = await getApiKey()
    const {signature} = await getSignature (payload)
    const body = {
        type,
        pair,
        side,
        nonce,
        price,
        amount,
    }
    
    return axios.post(`${host}/orders/`,body, {
        headers: {
          Authorization: apiKey,
          Signature: signature
        }
    })
}