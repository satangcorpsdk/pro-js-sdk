const axios = require('axios')
const {getHost} = require('../client/client')

exports.listOrderBook = async ({pair,limit}) => {
    const {host} = await getHost()
    return axios.get(`${host}/orders/?pair=${pair}`,{
        headers: {
            Accept: 'application/json',
        },
        params: {
            pair,
            limit,
        }
    })
}