
exports.handPromiseFunc = async function handPromiseFunc(promise) {
    return promise.then(resp => {
            return {
                error: null,
                result: resp.data
            }
    }).catch(err => {
        return {
            error: err
        }
    })
}