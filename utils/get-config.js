/**
 * Get configuration JSON based on NODE_ENV environment variable.
 *
 * @param {String} name  A configuration name, such as credentials.
 *
 * @returns {Object} A configuration object.
 **/
const POSSIBLE_ENVS = {
    development: true,
    production: true,
    test: true
}
exports.getConfig = function getConfig (name) {
    const env = process.env.NODE_ENV

    if (!POSSIBLE_ENVS[env]) {
      throw new Error(`Unrecognized environment NODE_ENV: ${env}`)
    }
  
    const config = require(`${process.cwd()}/config/${name}`)[env]
    if (!config) {
      throw new Error(`No configuration found for environment NODE_ENV: ${env}`)
    }
  
    return config
  }