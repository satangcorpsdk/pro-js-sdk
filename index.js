const  {getSignature, getApiKey} = require('./client/client')
const {handPromiseFunc} = require('./utils/promise-handle')
const {createOrder} = require('./order/create')
const {cancelOrder} = require('./order/cancel')
const {listOrder} = require('./order/list_order')
const {listOrderBook} = require('./order/list_order_book')
const {getUserAccount} = require('./user/get-user-account')
async function  main() {
    const {result: createOrderResp,error: createOrderErr} = await handPromiseFunc(createOrder({
        type: 'limit',
        pair: 'btc_thb',
        side: 'sell',
        price: '251500.00',
        amount: '0.0015'
    }))

    const {result: cancelOrderResp,error: cancelOrderErr} = await handPromiseFunc(cancelOrder({
        orderID: 21362317,
        pair: 'btc_thb'
    }))

    const {result: listBookOrders, error: listBookOrderErr} = await handPromiseFunc(listOrderBook({pair: "btc_thb",limit: 50}))

    const {result: listOrderResp, error: listOrderErr} = await handPromiseFunc(listOrder({
        pair: "btc_thb",
        status: "open",
        side: "sell",
        limit: "50",
        offset: "0"
    }))
    const {result: userAccountResp, error: getUserAccountErr} = await handPromiseFunc(getUserAccount())

}(async () => {
    await main()
 })()