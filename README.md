# pro-go-sdk


## SDK Features

## 1. handPromiseFunc

Handle response from promise function.
```
const {result, error} = await handPromiseFunc(new Promise((resolve,reject)=> {
    if (success) {
        return resolve(resp)
    } else {
        return reject(error)
    }
}))
```

## 2. Create
Call example.
```
const {result: createOrderResp,error: createOrderErr} = await handPromiseFunc(createOrder({
    type: 'limit',
    pair: 'btc_thb',
    side: 'sell',
    price: '251500.00',
    amount: '0.0015'
}))

```


## 3. Cancel
Call example

```
const {result: cancelOrderResp,error: cancelOrderErr} = await handPromiseFunc(cancelOrder({
    orderID: 21362317,
    pair: 'btc_thb'
}))
```


## 4. List user's orders

Call example

```
const {result: listOrderResp, error: listOrderErr} = await handPromiseFunc(listOrder({
    pair: "btc_thb",
    status: "open",
    side: "sell",
    limit: "50",
    offset: "0"
}))
```

## 5. List book orders

Call example

```
const {result: listBookOrders, error: listBookOrderErr} = await handPromiseFunc(listOrderBook({pair: "btc_thb",limit: 50}))
```

## 6. Get user account

Call example

```
const {result: userAccountResp, error: getUserAccountErr} = await handPromiseFunc(getUserAccount())
```

